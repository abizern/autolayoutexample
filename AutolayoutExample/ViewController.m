//
//  ViewController.m
//  AutolayoutExample
//
//  Created by Abizer Nasir on 08/10/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Create the containing view
	UIView *greyView = [[UIView alloc] initWithFrame:CGRectZero];
    greyView.backgroundColor = [UIColor grayColor];
    greyView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:greyView];

    // Create the label to go into the containing view
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.text = @"Example Text";
    label.textAlignment = NSTextAlignmentCenter;

    label.backgroundColor = [UIColor yellowColor];

    [greyView addSubview:label];
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(greyView, label);

    // Set the constraints for the greyView relative to it's superview - to cover completely
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[greyView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:bindings]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[greyView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:bindings]];

    // Set the constraints for the label to be pinned equally with padding
    [greyView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(30)-[label]-(30)-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:bindings]];
    [greyView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(30)-[label]-(30)-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:bindings]];

}

@end
